import React from 'react';
import Navbar from './components/navbar';
import './styles/App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import Home from "./components/pages/home"

function App() {
  return (
    <React.Fragment>
      <Router>
        <Navbar/>
        <Switch>
          <Route exact path={"/"} component={Home}/>
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
