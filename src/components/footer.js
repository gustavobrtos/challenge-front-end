import React, {useState} from 'react'
import "../styles/footer.css"
import { Link } from "react-router-dom"
import {Button, message} from "antd"
function Footer () {


  const [email, setEmail] = useState({})

  return (
    <div className={"footer-container"}>
      <section className={"footer-subscription"}>
        <p className={"footer-subscription-heading"}>
          Keep in touch with us
        </p>
        <p className={"footer-subscription-text"}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ut vehicula eros, eu consectetur est. 
        </p>
        <div className={"input-areas"}>
          <form>
            <input onChange={(e) => setEmail({email: e.target.value})} type={"email"} name={"email"} 
              className={"footer-input"}
              placeholder={"Enter your email to update"}
            />
            <Button type={"primary"} onClick={() => message.success(`Email ${email.email} cadastrado`)} style={{backgroundColor: "#fcdb00", borderColor: "#fcdb00"}} > Submit </Button>
          </form>
        </div>
      </section>

      <section className={"social-media"}>

        <div className={"social-icons"}>
          <Link className={"social-icon-link facebook"}>
            <i className={"fab fa-facebook-f"}></i>
          </Link>
          <Link className={"social-icon-link instagram"}>
            <i className={"fab fa-instagram"}></i>
          </Link>
          <Link className={"social-icon-link pinterest"}>
            <i className={"fab fa-pinterest"}></i>
          </Link>
          <Link className={"social-icon-link twitter"}>
            <i className={"fab fa-twitter"}></i>
          </Link>

          </div>

        </section>

      <div className={"footer-links"}>

        <div className={"footer-link-items"}>
          <p> Alameda Santos, 1970 </p>

        </div>
        <div className={"footer-link-items"}>
          <p> Alameda Santos, 1970 </p>
          
        </div>
        <div className={"footer-link-items"}>
          <p> Alameda Santos, 1970 </p>
          
        </div>
        <div className={"footer-link-items"}>
          <p> Alameda Santos, 1970 </p>
          
        </div>
        <div className={"footer-link-items"}>
          <p> Alameda Santos, 1970 </p>
          
        </div>

      </div>
      
    </div>
  )
}

export default Footer 