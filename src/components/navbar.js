import React, {useState} from 'react'
import { Link } from "react-router-dom"
import "../styles/navbar.css"
function Navbar() {

  const [click, setClick] = useState(false)

  const handleClick = () => setClick(!click)
  const closeMobileMenu = () => setClick(false)


  return (
    <React.Fragment>
      <nav className={"navbar"}> 
        <div className={"navbar-container"}>

          <Link to={"/"} className={"navbar-logo"}> 
            PIXTER
          </Link>

          <div className={"menu-icon"} onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars" }/>
          </div>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className={"nav-item"}>
               <Link to={"/"} className="nav-links" onClick={closeMobileMenu}>
                 Books
               </Link>
            </li>
            <li className={"nav-item"}>
               <Link to={"/"} className="nav-links" onClick={closeMobileMenu}>
                 Newsletter
               </Link>
            </li>
            <li className={"nav-item"}>
               <Link to={"/"} className="nav-links" onClick={closeMobileMenu}>
                 Adress
               </Link>
            </li>
          </ul>

        </div>
      </nav>
    </React.Fragment>
  )
}

export default Navbar
