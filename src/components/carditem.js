import React, {useState, useEffect} from 'react'
import {Card, Modal, Button,} from "antd"
import "../styles/Card.css"
const {Meta} = Card

function Carditem() {

  const [books, setBooks] = useState(false)
  const [modalState, setModalState] = useState({visible: false, bookDetails: {}})


  useEffect(() => {
    async function fetchData() {
      const res = await fetch("https://www.googleapis.com/books/v1/volumes?q=HARRY%20POTTER");
      res.json()
        .then(res => setBooks(res))
    }

    fetchData();
  },[]);

  let booksInfo = []

  if(books){


    booksInfo = books.items.map((item) => {

      return (


           <Card
            bordered={false}
            hoverable
            style={{margin: "20px", minWidth: "280px"}}
            cover={
              <img
                src={item.volumeInfo.imageLinks ? item.volumeInfo.imageLinks.thumbnail : ""}
              />
            }
            actions={[

              <Button
                style={{backgroundColor: "#fcdb00", borderColor: "#fcdb00"}}
                onClick={() => setModalState({visible: true, bookDetails: item})}
                type={"primary"}
              > 
                Ver mais detalhes

              </Button>

            ]}
          >
            <Meta
              title={item.volumeInfo.title}
              description={item.volumeInfo.subtitle}
            />
          </Card>

         
          
  
      

      )

    })
  }



  return (
    <div className={"items-grid"}>
      <Modal
        footer={[
          <Button onClick={ () => setModalState({visible: false, bookDetails: {}})} style={{backgroundColor: "#fcdb00", borderColor: "#fcdb00", color: "white"}} >
            Voltar
          </Button>
        ]}
        title={`Ver mais detalhes do livro: ${modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.title : ""}`}
        visible={modalState.visible}
        onCancel={() => setModalState({visible: false, bookDetails: {}})}
      > 

        <h4>Autores</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.authors.map(e => ` ${e}, ` ) : ""}</p>
        <h4>Descrição</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.description : ""}</p>
        <h4>Quantidade de páginas</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.pageCount : ""}</p>
        <h4>Data de publicação</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.publishedDate : ""}</p>
        <h4>Editora</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.publisher : ""}</p>
        <h4>Língua</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.language : ""}</p>
        <h4>Nota média</h4>
        <p> { modalState.bookDetails.volumeInfo ? modalState.bookDetails.volumeInfo.averageRating : ""}</p>


      </Modal>
      {booksInfo}
    </div>
  )
}

export default Carditem
