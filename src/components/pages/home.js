import React from 'react';
import "../../styles/App.css"
import HeaderSection from "../headersection"
import Footer from "../footer"
import Cards from "../cards"
function Home() {


  return( 
    <React.Fragment>
      <HeaderSection/>
      <Cards/>
      <Footer/>
    </React.Fragment>
  )

}

export default Home